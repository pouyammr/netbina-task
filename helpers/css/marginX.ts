const marginX = (margin: string) => {
  return {
    marginLeft: margin,
    marginRight: margin,
  }
}

export default marginX;