const paddingX = (padding: string) => {
  return {
    paddingLeft: padding,
    paddingRight: padding,
  }
}

export default paddingX;