const marginY = (margin: string) => {
  return {
    marginTop: margin,
    marginBottom: margin,
  }
}

export default marginY;