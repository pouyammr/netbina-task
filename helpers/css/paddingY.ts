const paddingY = (padding: string) => {
  return {
    paddingTop: padding,
    paddingBottom: padding,
  }
}

export default paddingY;