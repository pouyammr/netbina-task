import axios from 'axios';

const translation = axios.create({
  baseURL: "https://api.mymemory.translated.net",
  headers: { "Access-Control-Allow-Origin" : "*"}  
}
)

export default translation;