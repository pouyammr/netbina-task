import React, { useEffect, useState } from 'react'
import { InferGetServerSidePropsType } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import styles from '../styles/Home.module.css'

import { Box } from '@material-ui/core'

import Header from 'src/containers/Header'
import Footer from 'src/containers/Footer'
import ResultCard from 'src/components/ResultCard'
import TranslationCard from 'src/components/TranslationCard'

import translation from './api/translation'
import useSyncState from 'hooks/useSyncState'

type PageProps = {
  result: string,
  dir: "ltr" | "rtl",
}

const Home: React.FC<InferGetServerSidePropsType<typeof getServerSideProps>> = ({result, dir}) => {
  const [input, setInput] = useState<string>("");
  const [langPair, setLangPair] = useState<"en|fa" | "fa|en">("en|fa");
  const [loading, setLoading] = useSyncState(false);
  const [isCopied, setIsCopied] = useState<boolean>(false);

  const english = /[a-z]|[A-Z]/g
  
  const router = useRouter();
  useEffect(() => {
    english.test(input)
    ? setLangPair("en|fa")
    : setLangPair("fa|en")
  },[input])

  useEffect(() => {
    setLoading(true)
    const timeoutId = setTimeout(() => {
      setLoading(false);
      router.replace(`/?q=${input}&langpair=${langPair}`);
    }, 1500);
    return (() => clearTimeout(timeoutId));
  }, [input])
  
  return (
    <div className={styles.container}>
      <Head>
        <title>En to Fa Translator</title>
        <meta name="description" content="En to Fa Translator" />
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main className={styles.main}>
        <Box className={styles['box']}>
          <ResultCard loading={loading} direction={dir} changeCopied={(newValue: boolean) => setIsCopied(newValue)}>{result}</ResultCard>
          <TranslationCard input={input} changeInput={(newValue: string) => setInput(newValue)} direction={dir} />
        </Box>
      </main>
      <Footer />
    </div>
  )
};

export const getServerSideProps = async (context) => {
  const {q, langpair} = context.query;
  const res = await translation.get(encodeURI(`/get?q=${q}&langpair=${langpair}`));
  const result = res.data.responseStatus === 200 ? res.data.responseData.translatedText : "";
  const dir = langpair === "en|fa" ? "ltr" : "rtl";

  const data: PageProps = {
    result,
    dir,
  };
  
  return {
    props: data,
  }  
}

export default Home;
