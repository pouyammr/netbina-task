import '../styles/globals.css'
import 'public/fonts/Typewriter.css';
import 'public/fonts/Vazir.css';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
