import React, { useRef } from 'react';
import useStyles from './TranslationCard';
import {Paper, Box, IconButton, Typography} from '@material-ui/core';
import { useMediaQuery, useTheme } from '@material-ui/core';
import CloseRounded from '@material-ui/icons/CloseRounded';

interface TranslationCardProps {
  input: string,
  changeInput: (newValue: string) => void,
  direction: "rtl" | "ltr"
}

const TranslationCard: React.FC<TranslationCardProps> = ({ input, changeInput, direction }) => {
  const inputRef = useRef<HTMLInputElement>();

  const clearText = () => {
    changeInput("");
    inputRef.current.value = "";
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement|HTMLTextAreaElement>) => {
    changeInput(event.currentTarget.value);
  }

  const theme = useTheme();
  const [isXs, isMd] = [useMediaQuery(theme.breakpoints.down("md")), useMediaQuery(theme.breakpoints.up("md"))]
  const styles = useStyles(isXs, isMd, direction)();
  return (
    <Paper variant="elevation" className={styles['container']}>
      <Box className={styles['card']}>
          <Box className={styles['card-header']}><Typography className={styles['header-text']}>Enter Your Text:</Typography></Box>
          <Box className={styles['input']}>
          <input
          ref={inputRef} 
          className={styles['text-field']} 
          onChange={event => handleChange(event)} 
          placeholder="You can copy your text and paste it here or you can type it manually." />
            {input.length !== 0 && <IconButton size="medium" className={styles['clear']} onClick={clearText}>
              <CloseRounded />
            </IconButton>}
          </Box>
      </Box>
    </Paper>
  )
};

export default TranslationCard;

