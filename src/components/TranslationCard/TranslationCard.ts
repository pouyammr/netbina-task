import { makeStyles } from "@material-ui/styles";
import { teal } from "@material-ui/core/colors";
import paddingY from "helpers/css/paddingY";

const useStyles = (isXs: boolean, isMd: boolean, dir: "ltr" | "rtl") => makeStyles({
  'container': {
    width: isXs ? "100%" : isMd ? "53.4rem" : undefined,
    height: "100%",
  },
  'card': {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  'card-header': {
    width: "100%",
    height: "4.5rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingLeft: "1.5rem",
    backgroundColor: teal['500'],
  },
  'header-text': {
    fontFamily: "Bohemian Typewriter",
    fontSize: "1.3rem",
    color: "#fff"
  },
  'text-field': {
    width: "100%",
    ...paddingY('1.3rem'),
    paddingLeft: dir === "ltr" ? "1rem" : undefined,
    paddingRight: dir === "rtl" ? "1rem" : undefined,
    fontFamily: dir === "ltr" ? "Roboto" : "Vazir",
    fontSize: "1.3rem",
    fontWeight: "normal",
    outline: "none",
    border: "none",
    direction: dir,
  },
  'input': {
    position: "relative",
    width: "100%",
  },
  'clear': {
    position: "absolute",
    top: "0",
    left: dir === "rtl" ? "0" : undefined,
    right: dir === "ltr" ? "0" : undefined,
  }
});

export default useStyles;