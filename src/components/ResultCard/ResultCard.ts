import { makeStyles } from "@material-ui/styles";
import { teal } from "@material-ui/core/colors";

const useStyles = (isXs: boolean, isMd: boolean, dir: "ltr" | "rtl") => makeStyles({
  'container': {
    width: isXs ? "100%" : isMd ? "53.4rem" : undefined,
    height: "100%",
  },
  'card': {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column"
  },
  'card-header': {
    width: "100%",
    height: "4.5rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingLeft: "0.5rem",
    backgroundColor: teal['500'],
  },
  'result': {
    backgroundColor: "#d9dce1",
    display: "flex",
    height: "80%",
    paddingLeft: dir === "ltr" ? "1rem" : undefined,
    paddingRight: dir === "rtl" ? "1rem" : undefined,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    fontFamily: dir === "ltr" ? "Roboto" : "Vazir",
    direction: dir
  },
  'result-text': {
    fontFamily: dir === "ltr" ? "Roboto" : "Vazir",
    fontWeight: "normal",
    fontSize: "1.3rem",
    direction: dir
  }
});

export default useStyles;