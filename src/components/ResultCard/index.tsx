import React, { useRef } from 'react';
import useStyles from './ResultCard';
import { Paper, Box, IconButton} from '@material-ui/core';
import { useMediaQuery, useTheme } from '@material-ui/core';
import { FileCopyOutlined } from '@material-ui/icons';
import { BeatLoader } from 'react-spinners';

import { css } from 'styled-components';


interface ResultCardProps {
  changeCopied: (value: boolean) => void,
  loading: boolean,
  direction: "ltr" | "rtl",
  children: React.ReactNode
}

const spinner = css`
  margin-top: 1.3rem;
  margin-bottom: 1.3rem;
`

const ResultCard: React.FC<ResultCardProps> = ({ changeCopied, loading, direction, children}) => {
  const resultRef = useRef<HTMLParagraphElement>();

  const copyToClipboard = async () => {
    try {
      await navigator.clipboard.writeText(resultRef.current.innerHTML);
      changeCopied(true);
      setTimeout(() => changeCopied(false), 5000);
    }
    catch (error) {
      console.log(error)
    }; 
  };
  const theme = useTheme();
  const [isXs, isMd] = [useMediaQuery(theme.breakpoints.down("md")), useMediaQuery(theme.breakpoints.up("md"))];
  const resultDir = direction === "ltr" ? "rtl" : "ltr";
  const styles = useStyles(isXs, isMd, resultDir)();

  return (
    <Paper variant="elevation" className={styles['container']}>
      <Box className={styles['card']}>
        <Box className={styles['card-header']}>
          <IconButton size="medium" onClick={copyToClipboard}>
            <FileCopyOutlined />
          </IconButton>
        </Box>
        <Box className={styles['result']}>
          {loading
          ? <BeatLoader loading={loading} css={spinner} /> 
          : <p ref={resultRef} className={styles['result-text']}>{children}</p>}
        </Box>
      </Box>
    </Paper> 
  )
};

export default ResultCard;