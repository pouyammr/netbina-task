import React from 'react';
import Link from 'next/link';
import Image, { ImageProps } from 'next/image';

interface ImageLinkProps extends ImageProps {
  href: string,
  className?: string,
}

const ImageLink: React.FC<ImageLinkProps> = (props) => {
  const { href, className, ...rest} = props;

  return (
    <div className={className}>
      <Link href={href}>
        <Image {...rest} />
      </Link>
    </div>
  )
}

export default ImageLink;