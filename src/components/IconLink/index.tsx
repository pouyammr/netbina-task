import React from 'react';
import Link from 'next/link';

interface IconLinkProps {
  href: string,
  className?: string
}

const IconLink: React.FC<IconLinkProps> = (props) => {
  const { href, className} = props;

  return (
    <div className={className}>
      <Link href={href}>
          {props.children}
      </Link>
    </div>
  )
}

export default IconLink;