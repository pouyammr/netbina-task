import { makeStyles} from '@material-ui/core/styles';
import paddingY from 'helpers/css/paddingY';

const useStyles = makeStyles({
  'header': {
    display: 'flex',
    alignItems: 'center',
    ...paddingY('1rem'),
  },

  'icon': {
    margin: '0.75rem 1.5rem 0 0',
    cursor: 'pointer'
  },

  'header-title': {
    fontFamily: 'Bohemian Typewriter',
    userSelect: 'none'
  },

  'header-subtitle': {
    fontFamily: 'Bohemian Typewriter',
    userSelect: 'none'
  }
});

export default useStyles;