import React from 'react';
import useStyles from './Header';
import { Box, Typography, AppBar, Toolbar, createTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import { teal } from '@material-ui/core/colors';

import ImageLink from 'src/components/ImageLink';

import Typewriter from 'public/typewriter.svg';


const headerTheme = createTheme({
  palette: {
      primary: teal
  }
})

const typographyHeaderTheme = createTheme({
  palette: {
    text: {
      primary: '#3d0096',
      secondary: '#ffffff'
    }
  }
})
const Header: React.FC = () => {
  const styles = useStyles();
  return (
      <ThemeProvider theme={headerTheme}>
        <AppBar position="static" style={{width: '100vw'}}>
          <Toolbar className={styles['header']}>
            <ImageLink href="https://www.netbina.com" src={Typewriter} className={styles['icon']} width={50} height={50} />
            <Box flexDirection="column">
              <ThemeProvider theme={typographyHeaderTheme}>
                <Typography variant="h3" color="textPrimary" className={styles['header-title']}>NetBina</Typography>
                <Typography variant="h5" color="textSecondary" className={styles['header-subtitle']}>Online English To Farsi Translator</Typography>
              </ThemeProvider>
            </Box>
          </Toolbar>
        </AppBar>
      </ThemeProvider>
  )
}

export default Header;