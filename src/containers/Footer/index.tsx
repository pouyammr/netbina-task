import React from 'react';
import useStyles from './Footer';
import { AppBar, Toolbar, Box, Typography, createTheme, ThemeProvider, useTheme } from '@material-ui/core';
import { useMediaQuery } from '@material-ui/core';
import { teal } from '@material-ui/core/colors';

import IconLink from 'src/components/IconLink';
import ImageLink from 'src/components/ImageLink';

import { YouTube, Instagram, LinkedIn } from '@material-ui/icons';
import Aparat from 'public/aparat.svg';

const footerTheme = createTheme({
  palette: {
      primary: teal
  }
})

const typographyFooterTheme = createTheme({
  palette: {
    text: {
      primary: '#ffffff'
    }
  }
})

const logos = [
  {
    href: "https://www.youtube.com/channel/UC9oWrWW70YfVzDm3d6RwOGA",
    title: "NetBina YouTube",
    logo: <YouTube fontSize="large" />
  },
  {
    href: "https://www.instagram.com/netbina/",
    title: "NetBina Instagram",
    logo: <Instagram fontSize="large"/>
  },
  {
    href: "https://www.linkedin.com/company/netbina",
    title: "NetBina LinkedIn",
    logo: <LinkedIn fontSize="large"/>
  },
  {
    href: "https://www.aparat.com/Netbina/Netbina",
    title: "NetBina Aparat",
    logo: Aparat
  }
]

const Footer: React.FC = () => {
  const theme = useTheme();
  const [isMd, isSm, isXs] = [
    useMediaQuery(theme.breakpoints.up("md")),
    useMediaQuery(theme.breakpoints.only("sm")),
    useMediaQuery(theme.breakpoints.only("xs")),
  ]
  const styles = useStyles();
  return (
    <footer>
      <ThemeProvider theme={footerTheme}>
        <AppBar position="static" className={isXs ? styles['footer-xs'] : isSm ? styles['footer-xs'] : isMd && styles['footer']}>
          <Toolbar className={isXs ? styles['toolbar-xs'] : isSm ? styles['toolbar-xs'] : isMd && styles['toolbar']}>
            <Box textAlign={isXs ? "center" : isSm ? "center" : isMd ? "justify" : undefined} width={isXs ? "90%" : isSm ? "80%" : isMd && "60rem"}>
              <ThemeProvider theme={typographyFooterTheme}>
                <Typography color="textPrimary" className={styles['footer-text']}>How can we best introduce ourselves? Well, we were formed in 2006 as a digital marketing agency. 
                As brands have gained confidence in us over the years, we have grown to provide a much wider variety of services, but the focus of our 
                advertising activations remains digital, as that seems to give us the widest and most targeted market reach.
                Our service offerings are all in-house, inter-related and help to provide an integrated solution to our clients in shaping their strategy 
                and communication.</Typography>
              </ThemeProvider>
            </Box>
            <Box 
              display="flex" 
              flexDirection="row" 
              alignItems="center" 
              justifyContent="space-evenly" 
              width="15rem" 
              className={isXs ? styles['icon-box'] : isSm ? styles['icon-box'] : isMd && undefined}>
              {logos.map((el, index) => (
                index !== logos.length -1
                ? <IconLink href={el.href} key={el.title} className={styles['icon']}>{el.logo}</IconLink>
                : <ImageLink href={el.href} key={el.title} src={el.logo} className={styles['icon']} width={22} height={22} />
              ))}
            </Box>
          </Toolbar>
        </AppBar>
      </ThemeProvider>
    </footer>
  )
}

export default Footer;

