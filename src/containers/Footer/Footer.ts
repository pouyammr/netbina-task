import { makeStyles } from "@material-ui/core";
import paddingY from "helpers/css/paddingY";

const useStyles = makeStyles({
  'footer-xs': {
    width: '100vw',
    ...paddingY('1.5rem')
  },

  'footer': {
    width: '100vw',
    ...paddingY('1rem')
  },

  'toolbar-xs': {
    display: 'flex',
    flexDirection: 'column',
  },

  'toolbar': {
    display: 'flex',
    justifyContent: 'space-between',
  },

  'footer-text': {
    fontSize: '1.3rem',
    lineHeight: '1.8rem'
  },

  'icon-box': {
    marginTop: '1.5rem',
  },

  'icon': {
    cursor: 'pointer'
  }
});

export default useStyles;