import { useState } from "react";

const useSyncState = (initialValue?: any) => {
  const [value, updateValue] = useState<typeof initialValue>(initialValue);

  type nonReferenceType = string | number | boolean;

  let latestValue = value ?? null;

  const get = () => latestValue;

  const set = (newValue: unknown) => {
    if (Array.isArray(newValue))
      latestValue = [...newValue];
    else if (typeof newValue === "object" && newValue !== null)
      latestValue = {...newValue};
    else
      latestValue = newValue as nonReferenceType;
    updateValue(newValue);
    return latestValue;
  }

  return [get(), set];
}

export default useSyncState;